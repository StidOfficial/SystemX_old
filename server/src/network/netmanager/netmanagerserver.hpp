#ifndef __NETMANAGERSERVER_HPP__
#define __NETMANAGERSERVER_HPP__

#include <systemx/network/tcpipserver.hpp>

namespace Network
{
	namespace NetManager
	{
		class NetManagerServer : public TCPIPServer
		{
		public:
			NetManagerServer() : TCPIPServer() {}
			virtual void Open();
			virtual void OnAccept(int sSocket, sockaddr_in sAddr);
			virtual void Close();
		};
	}
}

#endif
