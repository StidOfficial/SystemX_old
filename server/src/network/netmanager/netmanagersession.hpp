#ifndef __NETMANAGERSESSION_HPP__
#define __NETMANAGERSESSION_HPP__

#include <systemx/network/tcpipsession.hpp>

#define IAC_COMMAND_LENGTH 3
#define IAC		255

#define DONT		254
#define DO		253
#define WONT		252
#define WILL		251

#define NAWS		31
#define TS		32
#define RFC		33
#define LINEMODE	34
#define NEO		39
#define XDL		35
#define TT		24
#define STATUS		5
#define SGA		3
#define ECHO		1

namespace Network
{
	namespace NetManager
	{
		class NetManagerSession : public TCPIPSession
		{
		public:
			NetManagerSession(int sSocket, sockaddr_in sAddr) : TCPIPSession(sSocket, sAddr) {}
			virtual void Open();
			virtual void ReceiveData();
			virtual void OnKeyPress(unsigned char keyCode);
			virtual void SendIAC(unsigned char command, unsigned char subcommand);
			virtual void Close();
		};
	}
}

#endif
