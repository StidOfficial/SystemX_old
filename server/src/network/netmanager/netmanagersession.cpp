#include "netmanagersession.hpp"

#include <systemx/logger.hpp>
#include <iostream>

#define TELNET_BUFFER_SIZE 254

namespace Network
{
	namespace NetManager
	{
		void NetManagerSession::Open()
		{
			LOG_INFO("[NetManagerSession] Connected !");
			TCPIPSession::Open();
		}

		void NetManagerSession::ReceiveData()
		{
			this->SendIAC(DO, SGA);
			this->SendIAC(WILL, SGA);
			this->SendIAC(WILL, ECHO);
			this->SendIAC(DONT, TT);
			this->SendIAC(DONT, NAWS);
			this->SendIAC(DONT, TS);
			this->SendIAC(DONT, RFC);
			this->SendIAC(DONT, LINEMODE);
			this->SendIAC(DONT, NEO);
			this->SendIAC(WILL, STATUS);
			this->SendIAC(DONT, XDL);
			
			this->Send("Username : ");

			unsigned char buffer[TELNET_BUFFER_SIZE];
			
			int leftSize;
			while(!this->sShutdown && (leftSize = recv(this->GetSocket(), &buffer, TELNET_BUFFER_SIZE, 0) > 0))
			{
				if(buffer[0] == (int)IAC)
				{
					std::cout << "IAC" << std::endl;
				}
				else
				{
					this->OnKeyPress(buffer[0]);
				}
			}
		}

		void NetManagerSession::OnKeyPress(unsigned char keyCode)
		{
			LOG_DEBUG("[NetManagerSession] %d", keyCode);
			this->Send((char*)&keyCode);
		}

		void NetManagerSession::SendIAC(unsigned char command, unsigned char subcommand)
		{
			const unsigned char IACCommand[IAC_COMMAND_LENGTH] = {
				IAC,
				command,
				subcommand
			};

			this->Send((const char*)IACCommand, IAC_COMMAND_LENGTH);
		}

		void NetManagerSession::Close()
		{
			LOG_INFO("[NetManagerSession] Disconnected !");
			TCPIPSession::Close();
		}
	}
}
