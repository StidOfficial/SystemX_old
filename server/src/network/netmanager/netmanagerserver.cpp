#include "netmanagerserver.hpp"
#include "netmanagersession.hpp"

#include <systemx/logger.hpp>
#include <iostream>

namespace Network
{
	namespace NetManager
	{
		void NetManagerServer::Open()
		{
			LOG_INFO("[NetManagerServer] Starting...");
			TCPIPServer::Open();
			LOG_INFO("[NetManagerServer] Started !");
		}

		void NetManagerServer::OnAccept(int sSocket, sockaddr_in sAddr)
		{
			Network::TCPIPSession *sSession = new NetManagerSession(sSocket, sAddr);
			auto it = this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
			sSession->Open();
			sSession->Close();
			delete sSession;

			this->sSessions.erase(it);
		}

		void NetManagerServer::Close()
		{
			LOG_INFO("[NetManagerServer] Closing...");
			TCPIPServer::Close();
			LOG_INFO("[NetManagerServer] Closed !");
		}
	}
}
