#ifndef __WEBMANAGERSERVER_HPP__
#define __WEBMANAGERSERVER_HPP__

#include <systemx/network/websocket/websocketserver.hpp>

namespace Network
{
	namespace WebManager
	{
		class WebManagerServer : public WebSocket::WebSocketServer
		{
		public:
			WebManagerServer() : WebSocket::WebSocketServer() {}
			virtual void Open();
			virtual void OnAccept(int sSocket, sockaddr_in sAddr);
			virtual void Close();
		};
	}
}

#endif
