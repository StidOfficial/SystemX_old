#include "webmanagerserver.hpp"
#include "webmanagersession.hpp"

#include <systemx/logger.hpp>

namespace Network
{
	namespace WebManager
	{
		void WebManagerServer::Open()
		{
			LOG_INFO("[WebAdminServer] Starting...");
			WebSocket::WebSocketServer::Open();
			LOG_INFO("[WebAdminServer] Started !");
		}

		void WebManagerServer::OnAccept(int sSocket, sockaddr_in sAddr)
		{
			Network::TCPIPSession *sSession = new WebManagerSession(sSocket, sAddr);
			this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
			sSession->Open();
			sSession->Close();

			auto it = this->sSessions.find(sSocket);
			if(it != this->sSessions.end())
				this->sSessions.erase(it);

			delete sSession;
		}

		void WebManagerServer::Close()
		{
			LOG_INFO("[WebAdminServer] Closing...");
			WebSocket::WebSocketServer::Close();
			LOG_INFO("[WebAdminServer] Closed !");
		}
	}
}
