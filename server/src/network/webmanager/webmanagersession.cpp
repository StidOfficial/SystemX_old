#include <systemx/network/systemx/packetmanager.hpp>

#include "webmanagersession.hpp"
#include <systemx/logger.hpp>

namespace Network
{
	namespace WebManager
	{
		WebManagerSession::WebManagerSession(int sSocket, sockaddr_in sAddr) : WebSocket::WebSocketSession(sSocket, sAddr)
		{
			m_user_id = -1;
		}

		void WebManagerSession::Open()
		{
			std::cout << "[WebAdminSession] Connected !" << std::endl;
			WebSocket::WebSocketSession::Open();
		}

		void WebManagerSession::SetUserId(int user_id)
		{
			m_user_id = user_id;
		}

		int WebManagerSession::GetUserId()
		{
			return m_user_id;
		}

		bool WebManagerSession::IsLogged()
		{
			return (m_user_id > 0);
		}

		void WebManagerSession::OnMessage(int16_t length, const unsigned char* payload)
		{
			if(length >= 2)
			{
				SystemX::PacketMessage* packetMessage = new SystemX::PacketMessage(payload, 0, length);

				std::cout << "[WebAdminSession] Recieved packet length " << packetMessage->GetLength() << " #" << packetMessage->GetId() << " !" << std::endl;

				SystemX::callback_t callback = SystemX::PacketManager::Find(packetMessage->GetId());

				if(callback == NULL)
				{
					std::cerr << "[WebAdminSession] Packet not found !" << std::endl;
				}
				else
					callback(this, packetMessage);

				delete packetMessage;
			}
			else
			{
				std::cerr << "[WebAdminSession] Packet too small !" << std::endl;
			}
		}

		void WebManagerSession::Send(SystemX::PacketMessage* packetMessage)
		{
			this->SendMessage(packetMessage->GetMessage(), packetMessage->GetLength());
		}

		void WebManagerSession::Close()
		{
			std::cout << "[WebAdminSession] Disconnected !" << std::endl;
			WebSocket::WebSocketSession::Close();
		}
	}
}
