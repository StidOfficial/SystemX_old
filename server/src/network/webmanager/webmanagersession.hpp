#ifndef __WEBMANAGERSESSION_HPP__
#define __WEBMANAGERSESSION_HPP__

#include <systemx/network/websocket/websocketsession.hpp>
#include <systemx/network/systemx/packetmessage.hpp>

namespace Network
{
	namespace WebManager
	{
		class WebManagerSession : public WebSocket::WebSocketSession
		{
		public:
			WebManagerSession(int sSocket, sockaddr_in sAddr);
			virtual void Open();
			void SetUserId(int user_id);
			int GetUserId();
			bool IsLogged();
			virtual void OnMessage(int16_t length, const unsigned char* payload);
			virtual void Send(SystemX::PacketMessage* packetMessage);
			virtual void Close();
		private:
			int m_user_id;
		};
	}
}

#endif
