#include <cstdlib>
#include <iostream>
#include <thread>
#include <signal.h>
#include <mutex>

#include <systemx/logger.hpp>
#include "systemx/systemx.hpp"
#include "network/webmanager/webmanagerserver.hpp"
#include "network/netmanager/netmanagerserver.hpp"

std::mutex mtx;

Network::TCPIPServer *webManagerServer, *netManagerServer;
void init_webmanager();
void init_netmanager();
void terminate(int sig);

int main(int argc, char *argv[])
{
	signal(SIGINT, terminate);

	SystemX::SystemX::Initialize();

	std::thread webManagerServer(init_webmanager);
	std::thread netManagerServer(init_netmanager);

	webManagerServer.join();
	netManagerServer.join();

	return EXIT_SUCCESS;
}

void init_webmanager()
{
	mtx.lock();
	webManagerServer = new Network::WebManager::WebManagerServer();
	webManagerServer->SetPort(8080);
	try
	{
		webManagerServer->Open();
		mtx.unlock();
		webManagerServer->Listen();
	}
	catch(std::string e)
	{
		LOG_ERROR("[WebManagerServer] %s", e);
	}

	mtx.lock();
	webManagerServer->Close();
	delete webManagerServer;
	mtx.unlock();
}

void init_netmanager()
{
	mtx.lock();
	netManagerServer = new Network::NetManager::NetManagerServer();
	netManagerServer->SetPort(8000);
	try
	{
		netManagerServer->Open();
		mtx.unlock();
		netManagerServer->Listen();
	}
	catch(std::string e)
	{
		LOG_ERROR("[WebManagerServer] %s", e);
	}

	mtx.lock();
	netManagerServer->Close();
	delete netManagerServer;
	mtx.unlock();
}

void terminate(int sig)
{
	if(sig == SIGINT)
	{
		webManagerServer->Shutdown();
		netManagerServer->Shutdown();

		SystemX::SystemX::Destroy();
	}
}
