#include "systemx.hpp"

#include "authentication.hpp"
#include "daemons.hpp"

namespace SystemX
{
	Database::Database* SystemX::db = NULL;

	void SystemX::Initialize()
	{
		db = new Database::Database(DATABASE_DRIVER_SQLITE);
		db->Open("systemx.db");
		db->Load("systemx.sql");

		Authentication::Initialize();
		Daemons::Initialize();
	}

	Database::Database* SystemX::GetDatabase()
	{
		return db;
	}

	void SystemX::Destroy()
	{
		delete db;
	}
}
