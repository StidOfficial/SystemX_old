#ifndef __DAEMONS_HPP__
#define __DAEMONS_HPP__

#include <systemx/network/websocket/websocketsession.hpp>
#include <systemx/network/systemx/packetmessage.hpp>
#include <map>

namespace SystemX
{
	typedef struct daemon_t {
		int id;
		unsigned char* address;
		int port;
	} daemon_t;

	class Daemons
	{
	public:
		static void Initialize();
		static void Add(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage);
		static void Remove(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage);
		static void List(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage);
	private:
		static std::map<int, daemon_t> m_daemons;
	};
}

#endif
