#include "daemons.hpp"

#include <systemx/network/systemx/packetmanager.hpp>
#include <systemx/database/database.hpp>
#include <systemx/database/preparedstatement.hpp>
#include <systemx/logger.hpp>
#include "systemx.hpp"

#define PACKET_DAEMONS_ADD 10
#define PACKET_DAEMONS_REMOVE 11
#define PACKET_DAEMONS_LIST 12

// Debug
#include <iostream>

namespace SystemX
{
	std::map<int, daemon_t> Daemons::m_daemons = std::map<int, daemon_t>();

	void Daemons::Initialize()
	{
		Network::SystemX::PacketManager::Register(PACKET_DAEMONS_ADD, Daemons::Add);
		Network::SystemX::PacketManager::Register(PACKET_DAEMONS_REMOVE, Daemons::Remove);
		Network::SystemX::PacketManager::Register(PACKET_DAEMONS_LIST, Daemons::List);

		Database::PreparedStatement* daemonPrepare = SystemX::GetDatabase()->Prepare("SELECT id, address, port FROM daemons;");
		daemonPrepare->Execute();

		while(daemonPrepare->Next())
		{
			int daemonId = daemonPrepare->GetInt("id");
			const unsigned char* daemonAddress = daemonPrepare->GetString("address");
			int daemonPort = daemonPrepare->GetInt("port");

			struct daemon_t daemon = {};
			daemon.address = (unsigned char*)daemonAddress;
			daemon.port = daemonPort;

			m_daemons.insert(std::pair<int, daemon_t>(daemonId, daemon));
		}

		delete daemonPrepare;
	}

	void Daemons::Add(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)
	{
		std::string daemon_address = packetMessage->GetString();
		uint16_t daemon_port = packetMessage->GetUint16();

		Database::PreparedStatement* daemonPrepare = SystemX::SystemX::GetDatabase()->Prepare("INSERT INTO daemons (address, port) VALUES (?, ?);");
		daemonPrepare->Bind(1, daemon_address);
		daemonPrepare->Bind(2, daemon_port);
		daemonPrepare->Execute();

		delete daemonPrepare;
	}

	void Daemons::Remove(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)
	{
		std::cout << "remove daemon" << std::endl;
	}

	void Daemons::List(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)
	{
		/*Network::SystemX::PacketMessage* daemonsMessage = new Network::SystemX::PacketMessage(255);

		daemonsMessage->SetUint16(m_daemons.size());

		for(std::map<int, daemon_t>::iterator it = m_daemons.begin(); it != m_daemons.end(); it++)
		{
			struct daemon_t daemon = (struct daemon_t)it->second;

			daemonsMessage->SetUint16(it->first);
			daemonsMessage->SetString(std::string((const char*)daemon.address));
			daemonsMessage->SetInt16(daemon.port);
		}

		session->SendMessage(daemonsMessage->GetMessage(), daemonsMessage->GetLength());*/
	}
}
