#include "authentication.hpp"
#include "../network/webmanager/webmanagersession.hpp"
#include "systemx.hpp"

#include <systemx/network/systemx/packetmanager.hpp>
#include <systemx/logger.hpp>

#define PACKET_AUTHENTICATION_LOGIN 1
#define PACKET_AUTHENTICATION_LOGOUT 2

#include <thread>
#include <chrono>

namespace SystemX
{
	void Authentication::Initialize()
	{
		Network::SystemX::PacketManager::Register(PACKET_AUTHENTICATION_LOGIN, Authentication::Login);
		Network::SystemX::PacketManager::Register(PACKET_AUTHENTICATION_LOGOUT, Authentication::Logout);
	}

	void Authentication::Login(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)
	{
		Network::WebManager::WebManagerSession* webSession = (Network::WebManager::WebManagerSession*)session;
		if(webSession->IsLogged())
			return;

		int authType = packetMessage->GetUint8();
		std::string username = packetMessage->GetString();
		std::string password = packetMessage->GetString();

		Network::SystemX::PacketMessage* returnMessage = new Network::SystemX::PacketMessage(1);
		returnMessage->SetId(PACKET_AUTHENTICATION_LOGIN);

		Database::PreparedStatement* loginPrepare = SystemX::SystemX::GetDatabase()->Prepare("SELECT id FROM users WHERE username = ? AND password = ?;");
		loginPrepare->Bind(1, username);
		loginPrepare->Bind(2, password);
		loginPrepare->Execute();

		if(loginPrepare->First())
		{
			LOG_DEBUG("%s is logged !", username.c_str());
			webSession->SetUserId(loginPrepare->GetInt("id"));
			returnMessage->SetBoolean(true);
		}
		else
		{
			LOG_DEBUG("%s invalid username or password !", username.c_str());
			returnMessage->SetBoolean(false);

			std::this_thread::sleep_for(std::chrono::seconds(5));
		}

		delete loginPrepare;

		session->SendMessage(returnMessage->GetMessage(), returnMessage->GetLength());
		delete returnMessage;
	}

	void Authentication::Logout(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)
	{
		Network::WebManager::WebManagerSession* webSession = (Network::WebManager::WebManagerSession*)session;
		if(!webSession->IsLogged())
			return;

		webSession->SetUserId(-1);
	}
}
