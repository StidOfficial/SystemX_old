#ifndef __AUTHENTICATION_HPP__
#define __AUTHENTICATION_HPP__

#include <systemx/network/websocket/websocketsession.hpp>
#include <systemx/network/systemx/packetmessage.hpp>

namespace SystemX
{
	class Authentication
	{
	public:
		static void Initialize();
		static void Login(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage);
		static void Logout(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage);
	};
}

#endif
