#ifndef __SYSTEMX_HPP__
#define __SYSTEMX_HPP__

#include <systemx/database/database.hpp>

namespace SystemX
{
	class SystemX
	{
	public:
		static void Initialize();
		static Database::Database* GetDatabase();
		static void Destroy();
	private:
		static Database::Database* db;
	};
}

#endif
