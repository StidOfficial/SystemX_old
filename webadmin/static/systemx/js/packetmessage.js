class PacketMessage {

	static get FLOAT32() {
		return 4;
	}

	static get FLOAT64() {
		return 8
	}

	static get INT32() {
		return 4;
	}

	static get INT16() {
		return 2;
	}

	static get INT8() {
		return 1;
	}

	static get UINT32() {
		return 4;
	}

	static get UINT16() {
		return 2;
	}

	static get UINT8() {
		return 1;
	}

	constructor(id, length) {
		this.index = PacketMessage.UINT16;

		if(length === undefined)
			this.dataview = new DataView(id);
		else {
			this.dataview = new DataView(new ArrayBuffer(PacketMessage.UINT16 + length));
			this.setId(id);
		}
	}

	setId(id) {
		this.setUint16(id, 0);
	}

	getId() {
		return this.getUint16(0);
	}

	setUint8(value, offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		this.dataview.setUint8(_offset, value);
		if(offset === undefined)
			this.index += PacketMessage.UINT8;
	}

	getUint8(offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		var value = this.dataview.getUint8(_offset);

		if(offset === undefined)
			this.index += PacketMessage.UINT8;

		return value;
	}

	setUint16(value, offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		this.dataview.setUint16(_offset, value);
		if(offset === undefined)
			this.index += PacketMessage.UINT16;
	}

	getUint16(offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		var value = this.dataview.getUint16(offset);

		if(offset === undefined)
			this.index += PacketMessage.UINT16;

		return value;
	}

	setBool(value, offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		this.setUint8(value, _offset);
	}

	getBool(offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		var value = (this.getUint8(_offset) == 0) ? false : true;

		if(offset === undefined)
			this.index += PacketMessage.UINT16;

		return value;
	}

	setString(value, offset) {
		if(offset === undefined)
			this.setUint16(value.length);
		else
			this.setUint16(value.length, offset);

		for (var i = 0; i < value.length; i++)
			if(offset === undefined)
				this.setUint8(value.charCodeAt(i));
			else
				this.setUint8(value.charCodeAt(i), offset + PacketMessage.UINT16 + (i * PacketMessage.UINT8));
	}

	getString(offset) {
		var _offset = (offset === undefined) ? this.index : offset;

		var size = -1;
		if(offset === undefined)
			size = this.getUint16();
		else
			size = this.getUint16(offset);

		var value = "";
		for (var i = 0; i < size; i++)
			if(offset === undefined)
				value += this.getUint8();
			else
				value += this.getUint8(offset + PacketMessage.UINT16 + (i * PacketMessage.UINT8));

		return value;
	}

	getMessage() {
		return this.dataview.buffer;
	}

	getLength() {
		return this.dataview.byteLength;
	}
}
