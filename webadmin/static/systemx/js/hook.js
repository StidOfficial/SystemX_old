class Hook {

	static initialize() {
		Hook.hooks = [];
	}

	static add(event_name, name, callback) {
		if(Hook.hooks[event_name] === undefined)
			Hook.hooks[event_name] = [];

		Hook.hooks[event_name][name] = callback;

		console.log("[Hook] " + event_name + "#" + name + " event added !");
	}

	static remove(event_name, name) {
		delete Hook.hooks[event_name][name];
	}

	static run(event_name, ...params) {
		for(var key in Hook.hooks[event_name]) {
			Hook.hooks[event_name][key].apply(this, params);
		}
	}
}
