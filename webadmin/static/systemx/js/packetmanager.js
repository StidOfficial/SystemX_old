class PacketManager {
	static initialize() {
		this.packetList = [];
	}

	static register(id, callback) {
		this.packetList[id] = callback;
	}

	static find(id) {
		return this.packetList[id];
	}
}
