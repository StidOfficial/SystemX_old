class Socket {
	static debugBuffer(arraybuffer) {
		var array = new Uint8Array(arraybuffer);

		var debug = "";
		for(var i = 0; i < array.length; i++) {
			if(array[i] > 32 && array[i] < 127)
				debug += String.fromCharCode(array[i]);
			else
				debug += "[" + array[i] + "]";
		}

		console.log("[SystemX][Socket][DebugBuffer] " + debug);
	}

	constructor() {
		this.retry = 0;
		this.maxRetry = 3;
	}

	getStatus() {
		if(!this.webSocket)
			return "disconnected";

		switch(this.webSocket.readyState) {
			case WebSocket.CONNECTING:
				return "connecting";
			case WebSocket.OPEN:
				return "connected";
			case WebSocket.CLOSING:
				return "disconnecting";
			case WebSocket.CLOSED:
				return "disconnected";
		}
	}

	open(url) {
		this.url = url;

		this.webSocket = new WebSocket(this.url);
		this.webSocket.binaryType = "arraybuffer";

		var socketClass = this;
		this.webSocket.addEventListener("open", function(event) {
			socketClass.onOpen(event);
		});
		this.webSocket.addEventListener("message", function(event) {
			socketClass.onMessage(event);
		});
		this.webSocket.addEventListener("error", function(event) {
			socketClass.onError(event);
		});
		this.webSocket.addEventListener("close", function(event) {
			socketClass.onClose(event);
		});
	}

	onOpen(event) {
		console.log("[SystemX][Socket] Connection established !");
	}

	onMessage(event) {
		if(event.data instanceof ArrayBuffer) {
			Socket.debugBuffer(event.data);

			var dataView = new DataView(event.data, 0);
			console.log("[SystemX][Socket] Recieved message #" + dataView.byteLength + " !");

			if(dataView.byteLength >= 2) {
				var packetId = dataView.getUint16(0);
				var callback = PacketManager.find(packetId);
				if(callback)
					callback(new PacketMessage(dataView.buffer));
				else
					console.error("[SystemX][Socket] Packet #" + packetId + " is not registered !");
			} else
				console.error("[SystemX][Socket] Packet too small !");
		} else
			console.error("[SystemX][Socket] " + (typeof event.data) + " message is not supported !");
	}

	onError(event) {
		console.error("[SystemX][Socket] Error #" + event.code +" !");
		if(this.retry <= this.maxRetry)
			this.open(this.url);
		else
			console.error("[SystemX][Socket] Max connection retry is reached !")

		this.retry++;
	}

	onClose(event) {
		console.log("[SystemX][Socket] Connetion closed #" + event.code + " !");
	}

	isOpen() {
		if(this.webSocket)
			return this.webSocket.readyState == WebSocket.OPEN;

		return false;
	}

	send(packetMessage) {
		var socket = this;
		var intervalId = setInterval(function() {
			if(socket.isOpen()) {
				clearInterval(intervalId);
				console.log("[SystemX][Socket] Send packet length " + packetMessage.getLength() + " #" + packetMessage.getId());
				Socket.debugBuffer(packetMessage.getMessage());
				socket.webSocket.send(packetMessage.getMessage());
			}
		}, 2);
	}

	close() {
		if(this.webSocket)
			this.webSocket.close();
	}
}
