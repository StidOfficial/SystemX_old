class Authentication {
	static get PACKET_LOGIN() {
		return 1;
	}

	static get SIMPLE_AUTH() {
		return 1;
	}

	static get PACKET_LOGOUT() {
		return 2;
	}

	constructor() {
		this.isLogged = false;

		var baseClass = this;
		PacketManager.register(Authentication.PACKET_LOGIN, function(message) {
			baseClass.onLogged(message);
		});
	}

	login(login, password, rememberMe) {
		var loginMessage = new PacketMessage(Authentication.PACKET_LOGIN, PacketMessage.UINT8 + PacketMessage.UINT16 + login.length + PacketMessage.UINT16 + password.length);
		loginMessage.setUint8(Authentication.SIMPLE_AUTH);
		loginMessage.setString(login);
		loginMessage.setString(password);

		SystemX.getInstance().getSocket().send(loginMessage);
	}

	onLogged(message) {
		this.isLogged = message.getBool();
		Hook.run("OnSignIn", this.isAuthentified());
	}

	isAuthentified() {
		return this.isLogged;
	}

	logout() {
		this.isLogged = false;

		var logoutMessage = new PacketMessage(Authentication.PACKET_LOGOUT, 0);
		SystemX.getInstance().getSocket().send(logoutMessage);
	}
}
