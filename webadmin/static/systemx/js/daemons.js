class Daemons {
	static get PACKET_DAEMONS_ADD() {
		return 10;
	}

	static get PACKET_DAEMONS_REMOVE() {
		return 11;
	}

	static get PACKET_DAEMONS_LIST() {
		return 12;
	}

	constructor() {
		var baseClass = this;
		PacketManager.register(10, function(message) {
			baseClass.onDaemonsList(message);
		});
	}

	list() {
		var listMessage = new PacketMessage(12, 0);
		SystemX.getInstance().getSocket().send(listMessage);
	}

	onDaemonsList(message) {
		var count = message.getUint8();
		for(i = 0; i < count; i++) {
			var address = message.getString();
			var port = message.getUint16();
			var connected =  message.getBoolean();

			console.log(address + " " + port + " " + connected);
		}

		Hook.run("OnDaemonsList", null);
	}

	add(address, port) {
		
	}
}
