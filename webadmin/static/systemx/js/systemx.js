class SystemX {
	constructor() {
	}

	initialize() {
		PacketManager.initialize();

		this.socket = new Socket();
		this.authentication = new Authentication();
		this.daemons = new Daemons();
		this.modules = [];
	}

	getSocket() {
		return this.socket;
	}

	getAuthentication() {
		return this.authentication;
	}

	getDaemons() {
		return this.daemons;
	}

	onStatusChanged(callback) {
		this.statusCallback = callback;
	}

	changeStatus(status) {
		if(this.statusCallback)
			this.statusCallback(status);
	}

	setTitle(title) {
		document.title = "SystemX - " + title;
	}

	setServer(serverAddress) {
		if(this.getSocket().isOpen())
			this.getSocket().close();

		this.getSocket().open("ws://" + serverAddress + ":8080");
	}

	submitSignOut(event, form) {
		console.log("[SystemX] Logout !");
		this.signOut();

		this.setTitle("Login");

		$(".container#login").show();
		$(".container#panel").hide();

		event.preventDefault();
	}

	load(page) {
		$.get({
			url: "static/systemx/html/" + page + ".html",
			dataType: "text",
			type: "GET"
		})
		.done(function(data) {
			console.log("[SystemX] Loaded page " + page);
			$(document.body).attr("page", page);
			$(document.body).html(data);
		})
		.fail(function() {
			console.error("[SystemX] Failed to load page " + page);
		});
	}

	registerModule(module) {
		console.log("[SystemX] " + module.id + " module is registered !");
		this.modules.push(module);
	}

	static getInstance() {
		if(!this.instance)
			this.instance = new SystemX();

		return this.instance;
	}
}
