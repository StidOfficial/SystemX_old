#include "daemonserver.hpp"
#include "daemonsession.hpp"

#include <iostream>

namespace Network
{
	namespace SystemX
	{
		void DaemonServer::Open()
		{
			std::cout << "[DaemonServer] Starting..." << std::endl;
			Network::TCPIPServer::Open();
			std::cout << "[DaemonServer] Started !" << std::endl;
		}

		void DaemonServer::OnAccept(int sSocket, sockaddr_in sAddr)
		{
			Network::TCPIPSession *sSession = new DaemonSession(sSocket, sAddr);
			this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
			sSession->Open();
			sSession->Close();

			auto it = this->sSessions.find(sSocket);
			if(it != this->sSessions.end())
				this->sSessions.erase(it);

			delete sSession;
		}

		void DaemonServer::Close()
		{
			std::cout << "[DaemonServer] Closing..." << std::endl;
			Network::TCPIPServer::Close();
			std::cout << "[DaemonServer] Closed !" << std::endl;
		}
	}
}
