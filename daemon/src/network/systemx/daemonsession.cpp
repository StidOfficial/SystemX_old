#include "daemonsession.hpp"

#define BUFFER_SIZE 4096

#include <iostream>

namespace Network
{
	namespace SystemX
	{
		void DaemonSession::Open()
		{
			std::cout << "[DaemonSession] Connected !" << std::endl;
			Network::TCPIPSession::Open();
		}

		void DaemonSession::ReceiveData()
		{
			this->sBuffer.resize(BUFFER_SIZE);

			int leftSize;
			while(!this->sShutdown && (leftSize = recv(this->GetSocket(), this->sBuffer.data(), this->sBuffer.size(), 0) > 0))
			{
				
			}
		}

		/*void DaemonSession::OnMessage(int16_t length, const unsigned char* payload)
		{
			if(length >= 2)
			{
				SystemX::PacketMessage* packetMessage = new SystemX::PacketMessage(payload, 0, length);

				std::cout << "[DaemonSession] Recieved packet length " << packetMessage->GetLength() << " #" << packetMessage->GetId() << " !" << std::endl;

				SystemX::callback_t callback = SystemX::PacketManager::Find(packetMessage->GetId());

				if(callback == NULL)
				{
					std::cerr << "[WebAdminSession] Packet not found !" << std::endl;
				}
				else
					callback(this, packetMessage);

				delete packetMessage;
			}
			else
			{
				std::cerr << "[WebAdminSession] Packet too small !" << std::endl;
			}
		}*/

		void DaemonSession::Send(SystemX::PacketMessage* packetMessage)
		{
			//this->Send((const char*)packetMessage->GetMessage(), (const int)packetMessage->GetLength());
		}

		void DaemonSession::Close()
		{
			std::cout << "[DaemonSession] Disconnected !" << std::endl;
			Network::TCPIPSession::Close();
		}
	}
}
