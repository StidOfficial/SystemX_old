#ifndef __DAEMONSESSION_HPP__
#define __DAEMONSESSION_HPP__

#include <systemx/network/tcpipsession.hpp>
#include <systemx/network/systemx/packetmessage.hpp>

namespace Network
{
	namespace SystemX
	{
		class DaemonSession : public Network::TCPIPSession
		{
		public:
			DaemonSession(int sSocket, sockaddr_in sAddr) : Network::TCPIPSession(sSocket, sAddr) {}
			virtual void Open();
			virtual void ReceiveData();
			//virtual void OnMessage(int16_t length, const unsigned char* payload);
			virtual void Send(SystemX::PacketMessage* packetMessage);
			virtual void Close();
		};
	}
}

#endif
