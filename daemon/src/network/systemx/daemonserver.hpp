#ifndef __DAEMONSERVER_HPP__
#define __DAEMONSERVER_HPP__

#include <systemx/network/tcpipserver.hpp>

namespace Network
{
	namespace SystemX
	{
		class DaemonServer : public Network::TCPIPServer
		{
		public:
			DaemonServer() : Network::TCPIPServer() {}
			virtual void Open();
			virtual void OnAccept(int sSocket, sockaddr_in sAddr);
			virtual void Close();
		};
	}
}

#endif
