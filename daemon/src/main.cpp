#include <cstdlib>
#include <iostream>
#include <csignal>

#include "network/systemx/daemonserver.hpp"
#include <systemx/logger.hpp>

Network::TCPIPServer *daemonServer;
void terminate(int sig);

int main(int argc, char *argv[])
{
	signal(SIGINT, terminate);

	daemonServer = new Network::SystemX::DaemonServer();
	daemonServer->SetPort(5353);
	try
	{
		daemonServer->Open();
		daemonServer->Listen();
	}
	catch(std::string e)
	{
		LOG_ERROR("[DaemonServer] %s", e);
	}

	daemonServer->Close();
	delete daemonServer;
	return EXIT_SUCCESS;
}

void terminate(int sig)
{
	if(sig == SIGINT)
	{
		daemonServer->Shutdown();
	}
}
