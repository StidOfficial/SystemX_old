cmake_minimum_required(VERSION 3.9)

file(GLOB_RECURSE SRCS src/*)

include_directories(include/)
add_library(${PROJECT_NAME} SHARED ${SRCS})

find_package(Threads)
find_package(OpenSSL REQUIRED)
#find_package(SQLite3 REQUIRED)
#find_package(MySQL REQUIRED)

target_link_libraries(${PROJECT_NAME} ${CMAKE_THREAD_LIBS_INIT} OpenSSL::Crypto sqlite3)
