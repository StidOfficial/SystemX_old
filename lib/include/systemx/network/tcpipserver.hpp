#ifndef __TCPSERVER_HPP__
#define __TCPSERVER_HPP__

#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <map>

#include "systemx/network/tcpipsession.hpp"

namespace Network
{
	struct sockaccept
	{
		void* sClass;
		int sSocket;
		sockaddr_in sAddr;
	};

	class TCPIPServer
	{
	public:
		TCPIPServer();
		void SetPort(const int port);
		int GetPort();
		virtual void Open();
		virtual void Listen();
		virtual void OnAccept(const int sSocket, const sockaddr_in sAddr);
		virtual void Shutdown();
		virtual void Close();
		std::map<int, TCPIPSession*> sSessions;
	protected:
		static void Accept(sockaccept sAccept);
	private:
		int sPort;
		int sBacklog;
		int sSocket;
		sockaddr_in sAddr;
		bool sShutdown;
	};
}

#endif
