#ifndef __TCPIPSESSION_HPP__
#define __TCPIPSESSION_HPP__

#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <string>
#include <stdexcept>
#include <vector>

#define BUFFER_MAX_SIZE 512

namespace Network
{
	class TCPIPSession
	{
	public:
		TCPIPSession(const int sSocket, const sockaddr_in sAddr);
		int GetSocket();
		char* GetAddress();
		u_short GetPort();
		virtual void Open();
		virtual void ReceiveData();
		virtual void Send(const char* data, const int length);
		virtual void Send(const char* data);
		virtual void Send(std::string data);
		virtual void Shutdown();
		virtual void Close();
		virtual ~TCPIPSession();
	protected:
		std::vector<unsigned char> sBuffer;
		bool sShutdown;
	private:
		int sSocket;
		sockaddr_in sAddr;
	};
}

#endif
