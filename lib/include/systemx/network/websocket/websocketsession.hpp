#ifndef __WEBSOCKETSESSION_HPP__
#define __WEBSOCKETSESSION_HPP__

#include "systemx/network/http/httpsession.hpp"
#include "systemx/network/http/httprequest.hpp"
#include "systemx/network/http/httpresponse.hpp"

namespace Network
{
	namespace WebSocket
	{
		class WebSocketSession : public HTTP::HTTPSession
		{
		public:
			WebSocketSession(int sSocket, sockaddr_in sAddr) : HTTP::HTTPSession(sSocket, sAddr) {};
			virtual void OnRequest();
			virtual void OnResponse();
			virtual void OnMessage(bool isFin, bool rsv1, bool rsv2, bool rsv3, int opcode, bool isMasked, int16_t length, const unsigned char* payload);
			virtual void OnMessage(int16_t length, const unsigned char* payload);
			void SendMessage(bool isFin, bool rsv1, bool rsv2, bool rsv3, int opcode, bool isMasked, int16_t length, const unsigned char* message);
			void SendMessage(const char* data, const int length);
			void SendMessage(const unsigned char* data, const int length);
			void SendMessage(const unsigned char* data, const int length, const int opcode);
			void Close(unsigned short statusCode);
			virtual void Close();

			static std::string GetOpcode(int opcode);
			static std::string GetStatusCode(unsigned short statusCode);
		};
	}
}
#endif
