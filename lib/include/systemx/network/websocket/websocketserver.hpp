#ifndef __WEBSOCKETSERVER_HPP__
#define __WEBSOCKETSERVER_HPP__

#include "systemx/network/http/httpserver.hpp"
#include "systemx/network/websocket/websocketsession.hpp"

#define WEBSOCKET_BLOB 1
#define WEBSOCKET_ARRAYBUFFER 2

namespace Network
{
	namespace WebSocket
	{
		class WebSocketServer : public HTTP::HTTPServer
		{
		public:
			WebSocketServer();
			void SetBinaryType(int binaryType);
			virtual void OnAccept(int sSocket, sockaddr_in sAddr);
		private:
			int binaryType;
		};
	}
}
#endif
