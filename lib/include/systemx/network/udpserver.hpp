#ifndef __UDPSERVER_HPP__
#define __UDPSERVER_HPP__

#include <sys/socket.h>
#include <arpa/inet.h>

namespace Network
{
	class UDPServer
	{
	public:
		UDPServer();
		int GetSocket();
		void SetPort(const int port);
		int GetPort();
		virtual void Open();
		virtual void Listen();
		virtual void OnMessage();
		virtual void Shutdown();
		virtual void Close();
	private:
		int sPort;
		int sBacklog;
		int sSocket;
		sockaddr_in sAddr;
		bool sShutdown;
	};
}

#endif

// http://www.cplusplus.com/forum/unices/76180/
