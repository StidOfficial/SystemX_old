#ifndef __HTTPHEADERS_HPP__
#define __HTTPHEADERS_HPP__

#include <string>
#include <map>

namespace Network
{
	namespace HTTP
	{
		class HTTPHeaders
		{
		public:
			void addHeader(std::string name, std::string value);
			std::string getHeader(std::string name);
			void setHeader(std::string name, std::string newValue);
			void removeHeader(std::string &name);
			std::map<std::string, std::string> &getMap();
		private:
			std::map<std::string, std::string> mHeaders;
		};
	}
}
#endif
