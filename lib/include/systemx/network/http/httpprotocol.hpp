#ifndef __HTTPPROTOCOL_HPP__
#define __HTTPPROTOCOL_HPP__

#include <string>

#define HTTP_VERSION_NULL		-0x1
#define HTTP_VERSION_0_9		0x09
#define HTTP_VERSION_1_0		0x10
#define HTTP_VERSION_1_1		0x11
#define HTTP_VERSION_2_0		0x20

namespace Network
{
	namespace HTTP
	{
		class HTTPProtocol
		{
		public:
			static int getProtocolVersion(std::string protocolVersion);
			static std::string getProtocolVersion(int protocolVersion);
		};
	}
}
#endif
