#ifndef __HTTPSESSION_HPP__
#define __HTTPSESSION_HPP__

#include <iostream>

#include "systemx/network/tcpipsession.hpp"
#include "systemx/network/http/httpprotocol.hpp"
#include "systemx/network/http/httprequest.hpp"
#include "systemx/network/http/httpresponse.hpp"

namespace Network
{
	namespace HTTP
	{
		class HTTPSession : public Network::TCPIPSession
		{
		public:
			HTTPSession(int sSocket, sockaddr_in sAddr);
			virtual void ReceiveData();
			HTTPRequest* &getRequest();
			HTTPResponse* &getResponse();
			virtual void Request();
			virtual void OnRequest();
			virtual void Response();
			virtual void OnResponse();
			virtual void Send(HTTPResponse* &httpResponse);
			virtual ~HTTPSession();
		private:
			HTTPRequest *request;
			HTTPResponse *response;
		};
	}
}
#endif
