#ifndef __HTTPRESPONSE_HPP__
#define __HTTPRESPONSE_HPP__

#include <string>

#include "systemx/network/http/httpheaders.hpp"

namespace Network
{
	namespace HTTP
	{
		class HTTPResponse
		{
		public:
			HTTPResponse();
			~HTTPResponse();
			void setProtocolVersion(std::string protocolVersion);
			void setProtocolVersion(int protocolVersion);
			int getProtocolVersion();
			std::string getProtocolVersionValue();
			void setStatusCode(int statusCode);
			int getStatusCode();
			std::string getStatusCodeValue();
			HTTPHeaders* &getHeaders();
			static std::string getStatusCode(int statusCode);
		private:
			HTTPHeaders* headers;
			int protocolVersion;
			int statusCode;
		};
	}
}
#endif
