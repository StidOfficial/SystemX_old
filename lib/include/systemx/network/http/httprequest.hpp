#ifndef __HTTPREQUEST_HPP__
#define __HTTPREQUEST_HPP__

#include <string>

#include "systemx/network/http/httpheaders.hpp"
#include "systemx/network/http/httpprotocol.hpp"

#define HTTP_METHOD_NULL		-0x1
#define HTTP_METHOD_GET			0x01
#define HTTP_METHOD_HEAD		0x02
#define HTTP_METHOD_POST		0x03
#define HTTP_METHOD_PUT			0x04
#define HTTP_METHOD_DELETE	0x05
#define HTTP_METHOD_CONNECT	0x06
#define HTTP_METHOD_OPTIONS	0x07
#define HTTP_METHOD_TRACE		0x08
#define HTTP_METHOD_PATCH		0x09

namespace Network
{
	namespace HTTP
	{
		class HTTPRequest
		{
		public:
			HTTPRequest();
			~HTTPRequest();
			void setMethod(std::string method);
			void setMethod(int method);
			int getMethod();
			std::string getMethodValue();
			void setURI(std::string uri);
			std::string getURI();
			void setProtocolVersion(std::string protocolVersion);
			void setProtocolVersion(int protocolVersion);
			int getProtocolVersion();
			std::string getProtocolVersionValue();
			HTTPHeaders* &getHeaders();
			static int getMethod(std::string method);
			static std::string getMethod(int method);
		private:
			HTTPHeaders* headers;
			int method;
			std::string URI;
			int protocolVersion;
		};
	}
}
#endif
