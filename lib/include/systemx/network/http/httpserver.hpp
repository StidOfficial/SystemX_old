#ifndef __HTTPSERVER_HPP__
#define __HTTPSERVER_HPP__

#include "systemx/network/tcpipserver.hpp"
#include "systemx/network/http/httpsession.hpp"

namespace Network
{
	namespace HTTP
	{
		class HTTPServer : public Network::TCPIPServer
		{
		public:
			HTTPServer() : Network::TCPIPServer() {}
			virtual void OnAccept(int sSocket, sockaddr_in sAddr);
		};
	}
}
#endif
