#ifndef __PACKETMESSAGE_HPP__
#define __PACKETMESSAGE_HPP__

#include <cstdint>
#include <string>
#include <vector>

namespace Network
{
	namespace SystemX
	{
		class PacketMessage
		{
		public:
			PacketMessage(int length);
			PacketMessage(const unsigned char* data);
			PacketMessage(const unsigned char* data, int offset);
			PacketMessage(const unsigned char* data, int offset, int length);
			void SetId(int id);
			int GetId();
			void SetInt8(int8_t value);
			void SetInt8(int8_t value, int offset);
			int8_t GetInt8();
			int8_t GetInt8(int offset);
			void SetInt16(int16_t value);
			void SetInt16(int16_t value, int offset);
			int16_t GetInt16();
			int16_t GetInt16(int offset);
			void SetInt32(int32_t value);
			void SetInt32(int32_t value, int offset);
			int32_t GetInt32();
			int32_t GetInt32(int offset);
			void SetUint8(uint8_t value);
			void SetUint8(uint8_t value, int offset);
			uint8_t GetUint8();
			uint8_t GetUint8(int offset);
			void SetUint16(uint16_t value);
			void SetUint16(uint16_t value, int offset);
			uint16_t GetUint16();
			uint16_t GetUint16(int offset);

			/* Not finish */
			uint32_t GetUint32(int offset);
			float GetFloat32(int offset);
			double GetFloat64(int offset);
			/* Not finish */

			void SetString(std::string value);
			void SetString(std::string value, int offset);
			std::string GetString();
			std::string GetString(int offset);
			void SetBoolean(bool value);
			void SetBoolean(bool value, int offset);
			bool GetBoolean();
			bool GetBoolean(int offset);
			int GetLength();
			const unsigned char* GetMessage();
		private:
			std::vector<unsigned char> m_buffer;
			int m_length;
			int m_offset;
		};
	}
}

#endif
