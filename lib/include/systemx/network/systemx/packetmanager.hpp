#ifndef __PACKETMANAGER_HPP__
#define __PACKETMANAGER_HPP__

#include <functional>
#include <map>

#include "systemx/network/websocket/websocketsession.hpp"
#include "systemx/network/systemx/packetmessage.hpp"

namespace Network
{
	namespace SystemX
	{
		typedef std::function<void(Network::WebSocket::WebSocketSession* session, Network::SystemX::PacketMessage* packetMessage)> callback_t;

		class PacketManager
		{
		public:
			static void Register(int packetId, callback_t callback);
			static callback_t Find(int packetId);
		private:
			static std::map<int, callback_t> packets;
		};
	}
}

#endif
