#ifndef __LOGGER_HPP__
#define __LOGGER_HPP__

#define LOG_PRINT(type, format, ...) \
	::Logger::Printf("[%s][%s:%d] " format "\n", type, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)

#define LOG_INFO(format, ...) \
	LOG_PRINT("INFO", format, ##__VA_ARGS__)

#define LOG_DEBUG(format, ...) \
	LOG_PRINT("DEBUG", format, ##__VA_ARGS__)

#define LOG_WARN(format, ...) \
	LOG_PRINT("WARN", format, ##__VA_ARGS__)

#define LOG_ERROR(format, ...) \
	LOG_PRINT("ERROR", format, ##__VA_ARGS__)

class Logger
{
public:
	static void Initiliaze();
	static void Printf(const char* format, ...);
	static void Close();
};

#endif
