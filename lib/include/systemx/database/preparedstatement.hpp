#ifndef __PREPARED_STATEMENT_HPP__
#define __PREPARED_STATEMENT_HPP__

#include <systemx/database/database.hpp>
#include <sqlite3.h>
#include <cstring>
#include <string>
#include <map>

namespace Database
{
	class Database;

	class PreparedStatement
	{
	public:
		PreparedStatement(Database* db, sqlite3_stmt* stmt);
		void Bind(int index, int value);
		void Bind(int index, std::string value);
		void Bind(int index, const char* value);
		void Bind(std::string index, std::string value);
		void Bind(const char* index, const char* value);
		int GetInt(const char* name);
		const unsigned char* GetString(const char* name);
		void Execute();
		bool First();
		bool Next();
		~PreparedStatement();
	private:
		Database* m_db;
		sqlite3_stmt* m_sqlite_stmt;
		std::map<const char*, int> m_columns;
	};
}

#endif
