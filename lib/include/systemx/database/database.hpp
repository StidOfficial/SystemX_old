#ifndef __DATABASE_HPP__
#define __DATABASE_HPP__

#include <systemx/database/preparedstatement.hpp>

#define DATABASE_DRIVER_SQLITE 1

#include <sqlite3.h>

namespace Database
{
	class PreparedStatement;

	class Database
	{
	public:
		Database(int driver);
		int Driver();
		const char* LastError();
		void Open(const char* path);
		void Load(const char* path);
		PreparedStatement* Prepare(const char* sql);
		void Query(const char* sql);
		void Exec(const char* sql, int (*callback)(void*,int,char**,char**));
		~Database();
	private:
		int m_driver_db;
		sqlite3* sqlite_db;

		static void profile(void* context, const char* sql, sqlite3_uint64 ns);
	};
}

#endif
