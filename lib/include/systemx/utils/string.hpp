#ifndef __STRING_HPP__
#define __STRING_HPP__

#include <vector>
#include <string>

namespace Utils
{
	class String
	{
	public:
		static const std::vector<std::string> Split(std::string str, std::string delimiter, int limit = -1);
	};
}
#endif
