#ifndef __CRYPTO_HPP__
#define __CRYPTO_HPP__

#include <string>

namespace Utils
{
	class Crypto
	{
	public:
		static const std::string SHA1(const std::string str);
		static const std::string EncodeBase64(const std::string data);
	};
}
#endif
