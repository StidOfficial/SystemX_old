#include <systemx/logger.hpp>

#include <cstdarg>
#include <cstdio>

void Logger::Initiliaze()
{
	
}

void Logger::Printf(const char* format, ...)
{
	va_list args;
	va_start(args, format);

	vprintf(format, args);

	va_end(args);
}

void Logger::Close()
{
	
}
