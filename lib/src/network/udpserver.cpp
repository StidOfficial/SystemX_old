#include "systemx/network/udpserver.hpp"

#include <string>
#include <cstring>
#include <unistd.h>

#include <iostream>

namespace Network
{
	UDPServer::UDPServer()
	{
		this->sShutdown = false;
		this->sPort = 1;
		this->sBacklog = 1;
	}

	int UDPServer::GetSocket()
	{
		return this->sSocket;
	}

	void UDPServer::SetPort(const int port)
	{
		this->sPort = port;
	}

	int UDPServer::GetPort()
	{
		return this->sPort;
	}

	void UDPServer::Open()
	{
		if((this->sSocket = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
			throw std::string("socket() failed : ") + strerror(errno);

		this->sAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		this->sAddr.sin_port = htons(this->sPort);
		this->sAddr.sin_family = AF_INET;

		if(bind(this->GetSocket(), (struct sockaddr*)&this->sAddr, sizeof(this->sAddr)) < 0)
			throw std::string("bind() failed : ") + strerror(errno);
	}

	void UDPServer::Listen()
	{
		int remainingSize;
		while(!this->sShutdown)
		{
			struct sockaddr_in client;
			socklen_t socklen = sizeof(client);
			char buffer[1024];
			remainingSize = recvfrom(this->GetSocket(), buffer, 1024, 0, (struct sockaddr*)&client, &socklen);
			std::cout << "qsd" << std::endl;
		}
	}

	void UDPServer::OnMessage()
	{
	
	}

	void UDPServer::Shutdown()
	{
		this->sShutdown = true;
		shutdown(this->sSocket, SHUT_RDWR);
	}

	void UDPServer::Close()
	{
		close(this->sSocket);
	}
}
