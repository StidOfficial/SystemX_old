#include "systemx/network/tcpipserver.hpp"

#include <string>
#include <thread>

namespace Network
{
	TCPIPServer::TCPIPServer()
	{
		this->sShutdown = false;
		this->sPort = -1;
		this->sBacklog = 1;
	}

	void TCPIPServer::SetPort(const int port)
	{
		this->sPort = port;
	}

	int TCPIPServer::GetPort()
	{
		return this->sPort;
	}

	void TCPIPServer::Open()
	{
		if((this->sSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
			throw std::string("socket() failed : ") + strerror(errno);

		this->sAddr.sin_addr.s_addr = htonl(INADDR_ANY);
		this->sAddr.sin_port = htons(this->sPort);
		this->sAddr.sin_family = AF_INET;

		const int sOptSO_REUSEADDR = 1;
		if(setsockopt(this->sSocket, SOL_SOCKET, SO_REUSEADDR, &sOptSO_REUSEADDR, sizeof(int)) == -1)
			throw std::string("setsockopt() : ") + strerror(errno);

		if(bind(this->sSocket, (struct sockaddr*)&this->sAddr, sizeof(this->sAddr)) < 0)
			throw std::string("bind() failed : ") + strerror(errno);
	}

	void TCPIPServer::Listen()
	{
		if(listen(this->sSocket, this->sBacklog))
			throw std::string("listen() failed : ") + strerror(errno);

		struct sockaddr_in cAddr;
		socklen_t cAddrSize = sizeof(cAddr);
		int cSocket;
		while(!this->sShutdown)
		{
			cSocket = accept(this->sSocket, (struct sockaddr*)&cAddr, &cAddrSize);
			if(this->sShutdown || cSocket < 0)
				break;

			sockaccept sAccept;
			sAccept.sClass = this;
			sAccept.sSocket = cSocket;
			sAccept.sAddr = cAddr;
			std::thread(this->Accept, sAccept).detach();
		}
	}

	void TCPIPServer::Accept(sockaccept sAccept)
	{
		TCPIPServer* server = ((TCPIPServer*)sAccept.sClass);
		server->OnAccept(sAccept.sSocket, sAccept.sAddr);
	}

	void TCPIPServer::OnAccept(int sSocket, sockaddr_in sAddr)
	{
		TCPIPSession *sSession = new TCPIPSession(sSocket, sAddr);
		auto it = this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
		sSession->Open();
		sSession->Close();
		delete sSession;

		this->sSessions.erase(it);
	}

	void TCPIPServer::Shutdown()
	{
		this->sShutdown = true;
		shutdown(this->sSocket, SHUT_RDWR);
	}

	void TCPIPServer::Close()
	{
		for(auto it = this->sSessions.begin(); it != this->sSessions.end();)
			(it++)->second->Shutdown();

		/*std::cout << this->sThreads.size() << std::endl;
		for(auto it = this->sThreads.begin(); it != this->sThreads.end(); it++)
		{
			std::thread thread = it;
		}*/

		close(this->sSocket);
	}
}
