#include "systemx/network/systemx/packetmanager.hpp"

namespace Network
{
	namespace SystemX
	{
		std::map<int, callback_t> PacketManager::packets;

		void PacketManager::Register(int packetId, callback_t callback)
		{
			PacketManager::packets.insert(PacketManager::packets.begin(), std::pair<int, callback_t>(packetId, callback));
		}

		callback_t PacketManager::Find(int packetId)
		{
			return PacketManager::packets[packetId];
		}
	}
}
