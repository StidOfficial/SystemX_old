#include "systemx/network/systemx/packetmessage.hpp"

#include <stdexcept>

namespace Network
{
	namespace SystemX
	{
		PacketMessage::PacketMessage(int length)
		{
			m_length = sizeof(uint16_t) + length;
			m_buffer = std::vector<unsigned char>();
			m_buffer.reserve(m_length);
			m_offset = sizeof(uint16_t);
		}

		PacketMessage::PacketMessage(const unsigned char* data)
		{
			PacketMessage(data, 0);
		}

		PacketMessage::PacketMessage(const unsigned char* data, int offset)
		{
			PacketMessage(data, offset, sizeof(data));
		}

		PacketMessage::PacketMessage(const unsigned char* data, int offset, int length)
		{
			m_buffer = std::vector<unsigned char>(data, data + length);
			m_length = length;
			m_offset = sizeof(uint16_t) + offset;
		}

		void PacketMessage::SetId(int id)
		{
			this->SetUint16(id, 0);
		}

		int PacketMessage::GetId()
		{
			return this->GetUint16(0);
		}

		void PacketMessage::SetInt8(int8_t value)
		{
			this->SetInt8(value, m_offset);
			m_offset += sizeof(int8_t);
		}

		void PacketMessage::SetInt8(int8_t value, int offset)
		{
			if(offset > m_length)
				throw std::out_of_range("Failed to set int8");

			m_buffer[offset] = value;
		}

		int8_t PacketMessage::GetInt8()
		{
			int8_t value = this->GetInt8(m_offset);
			m_offset += sizeof(int8_t);

			return value;
		}

		int8_t PacketMessage::GetInt8(int offset)
		{
			if(offset > m_length)
				throw std::out_of_range("Failed to get int8");

			return m_buffer[offset];
		}

		void PacketMessage::SetInt16(int16_t value)
		{
			this->SetInt16(value, m_offset);
			m_offset += sizeof(int16_t);
		}

		void PacketMessage::SetInt16(int16_t value, int offset)
		{

		}

		int16_t PacketMessage::GetInt16()
		{
			int16_t value = this->GetInt16(m_offset);
			m_offset += sizeof(int16_t);

			return value;
		}

		
		int16_t PacketMessage::GetInt16(int offset)
		{
			return -1;
		}

		void PacketMessage::SetUint8(uint8_t value)
		{
			this->SetUint8(value, m_offset);
			m_offset += sizeof(uint8_t);
		}

		void PacketMessage::SetUint8(uint8_t value, int offset)
		{
			if(offset > m_length)
				throw std::out_of_range("Failed to set uint8");

			m_buffer[offset] = value;
		}

		uint8_t PacketMessage::GetUint8()
		{
			uint8_t value = this->GetUint8(m_offset);
			m_offset += sizeof(uint8_t);

			return value;
		}

		uint8_t PacketMessage::GetUint8(int offset)
		{
			if(offset > m_length)
				throw std::out_of_range("Failed to get uint8");

			return m_buffer[offset];
		}

		void PacketMessage::SetUint16(uint16_t value)
		{
			this->SetUint16(value, m_offset);
			m_offset += sizeof(uint16_t);
		}

		void PacketMessage::SetUint16(uint16_t value, int offset)
		{
			if((offset + 1) > m_length)
				throw std::out_of_range("Failed to set uint16");

			m_buffer[offset] = (value >> 8) & 0xFF;
			m_buffer[offset + 1] = value & 0xFF;
		}

		uint16_t PacketMessage::GetUint16()
		{
			uint16_t value = this->GetUint16(m_offset);
			m_offset += sizeof(uint16_t);

			return value;
		}

		uint16_t PacketMessage::GetUint16(int offset)
		{
			if((offset + 1) > m_length)
				throw std::out_of_range("Failed to get uint16");

			return (m_buffer[offset] << 8) + m_buffer[offset + 1];
		}

		uint32_t PacketMessage::GetUint32(int offset)
		{
			if((offset + 1) > m_length)
				throw std::out_of_range("Failed to get uint32");

			return (m_buffer[offset] << 24) + (m_buffer[offset] << 16) + (m_buffer[offset] << 8) + m_buffer[offset + 1];
		}

		void PacketMessage::SetString(std::string value)
		{
			this->SetString(value, m_offset);
			m_offset += sizeof(uint16_t) + value.length();
		}

		void PacketMessage::SetString(std::string value, int offset)
		{
			this->SetUint16(offset);

			for(int i = 0; i < value.length(); i++)
			{
				this->SetUint8(value.at(i), offset + sizeof(uint16_t) + i);
			}
		}

		std::string PacketMessage::GetString()
		{
			std::string value = this->GetString(m_offset);
			m_offset += sizeof(uint16_t) + value.size();

			return value;
		}

		std::string PacketMessage::GetString(int offset)
		{
			std::string value = "";

			int length = this->GetUint16(offset);
			for(int i = 0; i < length; i++)
				value += this->GetUint8(offset + sizeof(uint16_t) + i);

			return value;
		}

		void PacketMessage::SetBoolean(bool value)
		{
			this->SetBoolean(value, m_offset);
			m_offset += sizeof(uint8_t);
		}

		void PacketMessage::SetBoolean(bool value, int offset)
		{
			this->SetUint8(value, offset);
		}

		bool PacketMessage::GetBoolean()
		{
			bool value = this->GetBoolean(m_offset);
			m_offset += sizeof(uint8_t);

			return value;
		}

		bool PacketMessage::GetBoolean(int offset)
		{
			return this->GetUint8(offset);
		}

		int PacketMessage::GetLength()
		{
			return m_length;
		}

		const unsigned char* PacketMessage::GetMessage()
		{
			return m_buffer.data();
		}
	}
}
