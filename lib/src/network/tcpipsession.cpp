#include "systemx/network/tcpipsession.hpp"

#include <iostream>

namespace Network
{
	TCPIPSession::TCPIPSession(int sSocket, sockaddr_in sAddr)
	{
		this->sSocket = sSocket;
		this->sAddr = sAddr;
		this->sShutdown = false;
	}

	int TCPIPSession::GetSocket()
	{
		return this->sSocket;
	}

	char* TCPIPSession::GetAddress()
	{
		return inet_ntoa(this->sAddr.sin_addr);
	}

	u_short TCPIPSession::GetPort()
	{
		return this->sAddr.sin_port;
	}

	void TCPIPSession::Open()
	{
		ReceiveData();
	}

	void TCPIPSession::ReceiveData()
	{
		this->sBuffer.reserve(BUFFER_MAX_SIZE);

		int remainingSize;
		while(!this->sShutdown)
		{
			this->sBuffer.resize(BUFFER_MAX_SIZE);
			remainingSize = recv(this->GetSocket(), this->sBuffer.data(), BUFFER_MAX_SIZE, 0);
			if(remainingSize <= 0)
				break;

			this->sBuffer.resize(remainingSize);
		}
	}

	void TCPIPSession::Send(const char* data, const int length)
	{
		if (send(this->GetSocket(), data, length, 0) < 0)
			throw std::string("send() failed : ") + strerror(errno);
	}

	void TCPIPSession::Send(const char* data)
	{
		this->Send(data, (int)strlen(data));
	}

	void TCPIPSession::Send(std::string data)
	{
		this->Send(data.c_str());
	}

	void TCPIPSession::Shutdown()
	{
		this->sShutdown = true;
		shutdown(this->GetSocket(), SHUT_RDWR);
	}
	
	void TCPIPSession::Close()
	{
		close(this->GetSocket());
	}

	TCPIPSession::~TCPIPSession()
	{
	}
}
