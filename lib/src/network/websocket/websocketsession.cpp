#include <cstdint>

#include "systemx/network/websocket/websocketsession.hpp"
#include "systemx/network/http/httpprotocol.hpp"

#include "systemx/utils/string.hpp"
#include "systemx/utils/crypto.hpp"

#define WEBSOCKET_MAGIC_STRING "258EAFA5-E914-47DA-95CA-C5AB0DC85B11"
#define WEBSOCKET_HEADER_MESSAGE_LENGTH 8

#define WEBSOCKET_OPCODE_CONTINUE	0x0
#define WEBSOCKET_OPCODE_TEXT		0x1
#define WEBSOCKET_OPCODE_BINARY		0x2
#define WEBSOCKET_OPCODE_CLOSE		0x8
#define WEBSOCKET_OPCODE_PING		0x9
#define WEBSOCKET_OPCODE_PONG		0xA

#define WEBSOCKET_CLOSE_STATUSCODE_NORMALE		1000
#define WEBSOCKET_CLOSE_STATUSCODE_GOING_AWAY		1001
#define WEBSOCKET_CLOSE_STATUSCODE_PROTOCOL_ERROR	1002
#define WEBSOCKET_CLOSE_STATUSCODE_UNSUPPORTED		1003
#define WEBSOCKET_CLOSE_STATUSCODE_CLOSE_NO_STATUS	1005
#define WEBSOCKET_CLOSE_STATUSCODE_CLOSE_ABNORMAL	1006
#define WEBSOCKET_CLOSE_STATUSCODE_CLOSE_TOO_LARGE	1009

#define WEBSOCKET_MASKING_KEY_LENGTH 4

namespace Network
{
	namespace WebSocket
	{
		void WebSocketSession::OnRequest()
		{
			HTTP::HTTPHeaders *httpRequestHeaders = this->getRequest()->getHeaders();
			if(this->getRequest()->getMethod() == HTTP_METHOD_GET && this->getRequest()->getURI() == "/" && httpRequestHeaders->getHeader("Upgrade") == "websocket" && !httpRequestHeaders->getHeader("Sec-WebSocket-Key").empty())
			{
				this->getResponse()->setStatusCode(101);
				HTTP::HTTPHeaders* httpResponseHeaders = this->getResponse()->getHeaders();
				httpResponseHeaders->addHeader("Upgrade", "websocket");
				httpResponseHeaders->addHeader("Connection", "Upgrade");
				httpResponseHeaders->addHeader("Sec-WebSocket-Accept", Utils::Crypto::EncodeBase64(Utils::Crypto::SHA1(httpRequestHeaders->getHeader("Sec-WebSocket-Key") + WEBSOCKET_MAGIC_STRING)));
				
				std::cout << "[WebSocketSession] Accepted !" << std::endl;
			}
		}
		
		void WebSocketSession::OnResponse()
		{
			if(this->getResponse()->getStatusCode() != 101)
				return;

			unsigned char byte[2];
			do
			{
				int result = recv(this->GetSocket(), byte, 2, 0);
				if(result == 0 || result == EINTR)
				{
					//std::cerr << strerror(errno) << std::endl;
					break;
				}

				bool isFin = ((byte[0] & 0x80) != 0);
				bool rsv1 = ((byte[0] & 0x40) != 0);
				bool rsv2 = ((byte[0] & 0x20) != 0);
				bool rsv3 = ((byte[0] & 0x10) != 0);
				int opcode = (byte[0] & 0x0F);

				bool isMasked = ((byte[1] & 0x80) != 0);
				int16_t payloadLength = (byte[1] & 0x7F);

				int extendedPayloadByteLength = 0;
				if(payloadLength == 0x7E)
					extendedPayloadByteLength = 2;
				else if(payloadLength == 0x7F)
					extendedPayloadByteLength = 8;

				if(extendedPayloadByteLength > 0)
				{
					payloadLength = 0;
					while(--extendedPayloadByteLength >= 0)
					{
						recv(this->GetSocket(), byte, 1, 0);
						payloadLength |= (byte[0] & 0xFF) << (8 * extendedPayloadByteLength);
					}
				}

				unsigned char maskingKey[WEBSOCKET_MASKING_KEY_LENGTH];
				if(isMasked)
					recv(this->GetSocket(), maskingKey, sizeof(maskingKey), 0);

				unsigned char payload[payloadLength];
				recv(this->GetSocket(), payload, payloadLength, 0);

				if(isMasked)
					for(int i = 0; i < sizeof(payload); i++)
						payload[i] ^= maskingKey[i % 4];

				this->OnMessage(isFin, rsv1, rsv2, rsv3, opcode, isMasked, payloadLength, payload);
				if(opcode == WEBSOCKET_OPCODE_CLOSE)
					this->Shutdown();
			}
			while(!this->sShutdown);
		}

		void WebSocketSession::OnMessage(bool isFin, bool rsv1, bool rsv2, bool rsv3, int opcode, bool isMasked, int16_t length, const unsigned char* payload)
		{
			std::cout << "[WebSocketSession] " << WebSocketSession::GetOpcode(opcode);
			if(isFin)
				std::cout << " [FIN]";
			if(isMasked)
				std::cout << " [MASKED]";
			std::cout << std::endl;

			if(opcode != WEBSOCKET_OPCODE_CLOSE)
				this->OnMessage(length, payload);
		}

		void WebSocketSession::OnMessage(int16_t length, const unsigned char* payload)
		{
		}

		void WebSocketSession::SendMessage(bool isFin, bool rsv1, bool rsv2, bool rsv3, int opcode, bool isMasked, int16_t length, const unsigned char* message)
		{
			std::vector<unsigned char> dataMessage;
			unsigned char block1;
			block1 |= isFin << 7;
			block1 |= rsv1 << 6;
			block1 |= rsv1 << 5;
			block1 |= rsv1 << 4;
			block1 |= opcode;
			dataMessage.push_back(block1);

			unsigned char block2;
			block2 |= length;
			block2 |= isMasked << 7;
			dataMessage.push_back(block2);

			if(message)
				dataMessage.insert(dataMessage.end(), message, message + length);

			TCPIPSession::Send((const char*)dataMessage.data(), dataMessage.size());
		}

		void WebSocketSession::SendMessage(const char* data, const int length)
		{
			this->SendMessage((const unsigned char*)data, length);
		}

		void WebSocketSession::SendMessage(const unsigned char* data, const int length)
		{
			this->SendMessage(data, length, WEBSOCKET_OPCODE_BINARY);
		}

		void WebSocketSession::SendMessage(const unsigned char* data, const int length, const int opcode)
		{
			this->SendMessage(true, false, false, false, opcode, false, length, data);
		}

		void WebSocketSession::Close(unsigned short statusCode)
		{
			this->SendMessage(true, false, false, false, WEBSOCKET_OPCODE_CLOSE, false, 2, (unsigned char*)(std::to_string(statusCode).c_str()));
		}

		void WebSocketSession::Close()
		{
			//this->Close(WEBSOCKET_CLOSE_STATUSCODE_GOING_AWAY);
			HTTP::HTTPSession::Close();
		}

		std::string WebSocketSession::GetOpcode(int opcode)
		{
			switch(opcode)
			{
				case WEBSOCKET_OPCODE_CONTINUE:
					return "Continuation";
				case WEBSOCKET_OPCODE_TEXT:
					return "Text";
				case WEBSOCKET_OPCODE_BINARY:
					return "Binary";
				case WEBSOCKET_OPCODE_CLOSE:
					return "Connection Close";
				case WEBSOCKET_OPCODE_PING:
					return "Ping";
				case WEBSOCKET_OPCODE_PONG:
					return "Pong";
				default:
					return NULL;
			}
		}

		std::string WebSocketSession::GetStatusCode(unsigned short statusCode)
		{
			switch(statusCode)
			{
				case WEBSOCKET_CLOSE_STATUSCODE_NORMALE:
					return "Normal Closure";
				case WEBSOCKET_CLOSE_STATUSCODE_GOING_AWAY:
					return "Going Away";
				case WEBSOCKET_CLOSE_STATUSCODE_PROTOCOL_ERROR:
					return "Protocol error";
				case WEBSOCKET_CLOSE_STATUSCODE_UNSUPPORTED:
					return "Unsupported Data";
				case 1004:
					return "---Reserved----";
				case WEBSOCKET_CLOSE_STATUSCODE_CLOSE_NO_STATUS:
					return "No Status Rcvd";
				case WEBSOCKET_CLOSE_STATUSCODE_CLOSE_ABNORMAL:
					return "Abnormal Closure";
				case 1007:
					return "Invalid frame payload data";
				case 1008:
					return "Policy Violation";
				case WEBSOCKET_CLOSE_STATUSCODE_CLOSE_TOO_LARGE:
					return "Message Too Big";
				case 1010:
					return "Mandatory Ext.";
				case 1011:
					return "Internal Server";
				case 1015:
					return "TLS handshake";
				default:
					return "Unknown";
			}
		}
	}
}
