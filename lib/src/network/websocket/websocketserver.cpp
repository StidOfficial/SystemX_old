#include "systemx/network/websocket/websocketserver.hpp"
#include "systemx/network/websocket/websocketsession.hpp"

namespace Network
{
	namespace WebSocket
	{
		WebSocketServer::WebSocketServer() : HTTP::HTTPServer()
		{
			this->binaryType = WEBSOCKET_BLOB;
		}

		void WebSocketServer::OnAccept(int sSocket, sockaddr_in sAddr)
		{
			Network::TCPIPSession *sSession = new WebSocketSession(sSocket, sAddr);
			auto it = this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
			sSession->Open();
			sSession->Close();
			delete sSession;

			this->sSessions.erase(it);
		}

		void WebSocketServer::SetBinaryType(int binaryType)
		{
			this->binaryType = binaryType;
		}
	}
}
