#include "systemx/network/http/httpheaders.hpp"

namespace Network
{
	namespace HTTP
	{
		void HTTPHeaders::addHeader(std::string name, std::string value)
		{
			this->mHeaders[name] = value;
		}

		std::string HTTPHeaders::getHeader(std::string name)
		{
			return this->mHeaders[name];
		}

		void HTTPHeaders::setHeader(std::string name, std::string newValue)
		{
			this->mHeaders[name] = newValue;
		}

		void HTTPHeaders::removeHeader(std::string &name)
		{
			this->mHeaders.erase(name);
		}

		std::map<std::string, std::string> &HTTPHeaders::getMap()
		{
			return this->mHeaders;
		}
	}
}
