#include "systemx/network/http/httpsession.hpp"
#include "systemx/network/http/httpprotocol.hpp"
#include "systemx/utils/string.hpp"
#include "systemx/utils/crypto.hpp"

#define HEADER_BUFFER_SIZE 1024
#define HEADER_DELIMITER "\r\n"
#define BUFFER_SIZE 4096

namespace Network
{
	namespace HTTP
	{
		HTTPSession::HTTPSession(int sSocket, sockaddr_in sAddr) : Network::TCPIPSession(sSocket, sAddr)
		{
			this->request = new HTTPRequest();
			this->response = new HTTPResponse();
		}

		void HTTPSession::ReceiveData()
		{
			this->sBuffer.resize(HEADER_BUFFER_SIZE);
			
			bool isRequestMethod = true;
			bool isRequestHeaders = false;
			std::string headerLine;
			
			int leftSize;
			while(!this->sShutdown && (leftSize = recv(this->GetSocket(), this->sBuffer.data(), this->sBuffer.size(), 0) > 0))
			{
				std::string data = std::string((const char*)this->sBuffer.data(), this->sBuffer.size());
				size_t headerLineStartPos = 0;
				size_t headerLineEndPos = std::string::npos;
				do
				{
					headerLineEndPos = data.find(HEADER_DELIMITER, headerLineStartPos);
					if(headerLineEndPos != std::string::npos)
					{
						headerLine.append(data, headerLineStartPos, (headerLineEndPos - headerLineStartPos));
						if(headerLine.empty())
						{
							this->Request();
							this->Response();
							break;
						}

						if(isRequestMethod)
						{
							std::vector<std::string> httpMethod = Utils::String::Split(headerLine, " ", 3);
							if(httpMethod.size() < 2)
							{
								this->Shutdown();
								std::cout << "HTTP method parse error !" << std::endl;
								break;
							}
							this->getRequest()->setMethod(httpMethod[0]);
							this->getRequest()->setURI(httpMethod[1]);
							if(httpMethod.size() == 3)
								this->getRequest()->setProtocolVersion(httpMethod[2]);
							isRequestMethod = false;
							isRequestHeaders = true;
						}
						else if(isRequestHeaders)
						{
							std::vector<std::string> httpHeader = Utils::String::Split(headerLine, ": ", 1);
							this->getRequest()->getHeaders()->addHeader(httpHeader[0], httpHeader[1]);
						}
						else
						{
							std::cerr << "Another type not supported !" << std::endl;
						}
						headerLine.clear();
					}

					headerLineStartPos = headerLineEndPos + strlen(HEADER_DELIMITER);
				}
				while(headerLineEndPos != std::string::npos);
			}
		}

		HTTPRequest* &HTTPSession::getRequest()
		{
			return this->request;
		}

		HTTPResponse* &HTTPSession::getResponse()
		{
			return this->response;
		}

		void HTTPSession::Request()
		{
			this->getResponse()->setProtocolVersion(HTTP_VERSION_1_1);
			
			if(this->getRequest()->getProtocolVersion() == HTTP_VERSION_NULL)
				this->getResponse()->setStatusCode(505);
			else
				if(this->request->getMethod() == HTTP_METHOD_NULL)
					this->getResponse()->setStatusCode(501);
				else
					if(this->getRequest()->getURI().length() > 255)
						this->getResponse()->setStatusCode(414);
					else
					{
						this->getResponse()->setStatusCode(405);
					}
			
			this->OnRequest();
		}

		void HTTPSession::OnRequest()
		{
		}
		
		void HTTPSession::Response()
		{
			this->Send(this->getResponse());
			this->OnResponse();

			this->Shutdown();
		}
		
		void HTTPSession::OnResponse()
		{
		}

		void HTTPSession::Send(HTTPResponse* &httpResponse)
		{
			TCPIPSession::Send(httpResponse->getProtocolVersionValue() + " " + std::to_string(httpResponse->getStatusCode()) + " " + httpResponse->getStatusCodeValue() + HEADER_DELIMITER);
			for(auto const &header : httpResponse->getHeaders()->getMap())
				TCPIPSession::Send(header.first + ": " + header.second + HEADER_DELIMITER);
			TCPIPSession::Send(HEADER_DELIMITER);
		}

		HTTPSession::~HTTPSession()
		{
			delete this->request;
			delete this->response;
		}
	}
}
