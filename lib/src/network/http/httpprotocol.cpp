#include "systemx/network/http/httpprotocol.hpp"

namespace Network
{
	namespace HTTP
	{
		int HTTPProtocol::getProtocolVersion(std::string protocolVersion)
		{
			if(protocolVersion == "HTTP/0.9")
				return HTTP_VERSION_0_9;
			else if(protocolVersion == "HTTP/1.0")
				return HTTP_VERSION_1_0;
			else if(protocolVersion == "HTTP/1.1")
				return HTTP_VERSION_1_1;
			else if(protocolVersion == "HTTP/2.0")
				return HTTP_VERSION_2_0;

			return -1;
		}

		std::string HTTPProtocol::getProtocolVersion(int protocolVersion)
		{
			switch(protocolVersion)
			{
				case HTTP_VERSION_0_9:
					return "HTTP/0.9";
				case HTTP_VERSION_1_0:
					return "HTTP/1.0";
				case HTTP_VERSION_1_1:
					return "HTTP/1.1";
				case HTTP_VERSION_2_0:
					return "HTTP/2.0";
				default:
					return NULL;
			}
		}
	}
}
