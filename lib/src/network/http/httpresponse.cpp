#include "systemx/network/http/httpresponse.hpp"
#include "systemx/network/http/httpprotocol.hpp"

namespace Network
{
	namespace HTTP
	{
		HTTPResponse::HTTPResponse()
		{
			this->headers = new HTTPHeaders();
			this->protocolVersion = -1;
			this->statusCode = -1;
		}

		void HTTPResponse::setProtocolVersion(std::string protocolVersion)
		{
			this->setProtocolVersion(HTTPProtocol::getProtocolVersion(protocolVersion));
		}

		void HTTPResponse::setProtocolVersion(int protocolVersion)
		{
			this->protocolVersion = protocolVersion;
		}

		int HTTPResponse::getProtocolVersion()
		{
			return this->protocolVersion;
		}

		std::string HTTPResponse::getProtocolVersionValue()
		{
			return HTTPProtocol::getProtocolVersion(this->getProtocolVersion());
		}

		void HTTPResponse::setStatusCode(int statusCode)
		{
			this->statusCode = statusCode;
		}

		int HTTPResponse::getStatusCode()
		{
			return this->statusCode;
		}

		std::string HTTPResponse::getStatusCodeValue()
		{
			return HTTPResponse::getStatusCode(this->getStatusCode());
		}

		HTTPHeaders* &HTTPResponse::getHeaders()
		{
			return this->headers;
		}

		std::string HTTPResponse::getStatusCode(int statusCode)
		{
			switch(statusCode)
			{
				case 100:
					return "Continue";
				case 101:
					return "Switching Protocols";
				case 102:
					return "Processing";
				case 200:
					return "OK";
				case 201:
					return "Created";
				case 202:
					return "Accepted";
				case 203:
					return "Non-Authoritative Information";
				case 204:
					return "No Content";
				case 205:
					return "Reset Content";
				case 206:
					return "Partial Content";
				case 207:
					return "Multi-Status";
				case 208:
					return "Already Reported";
				case 210:
					return "Content Different";
				case 226:
					return "IM Used";
				case 300:
					return "Multiple Choices";
				case 301:
					return "Moved Permanently";
				case 302:
					return "Moved Temporarily";
				case 303:
					return "See Other";
				case 304:
					return "Not Modified";
				case 305:
					return "Use Proxy";
				case 306:
					return "";
				case 307:
					return "Temporary Redirect";
				case 308:
					return "Permanent Redirect";
				case 310:
					return "Too many Redirects";
				case 400:
					return "Bad Request";
				case 401:
					return "Unauthorized";
				case 402:
					return "Payment Required";
				case 403:
					return "Forbidden";
				case 404:
					return "Not Found";
				case 405:
					return "Method Not Allowed";
				case 406:
					return "Not Acceptable";
				case 407:
					return "Proxy Authentication Required";
				case 408:
					return "Request Time-out";
				case 409:
					return "Conflict";
				case 410:
					return "Gone";
				case 411:
					return "Length Required";
				case 412:
					return "Precondition Failed";
				case 413:
					return "Request Entity Too Large";
				case 414:
					return "Request-URI Too Long";
				case 415:
					return "Unsupported Media Type";
				case 416:
					return "Requested range unsatisfiable";
				case 417:
					return "Expectation failed";
				case 418:
					return "I’m a teapot";
				case 421:
					return "Bad mapping / Misdirected Request";
				case 422:
					return "Unprocessable entity";
				case 423:
					return "Locked";
				case 424:
					return "Method failure";
				case 425:
					return "Unordered Collection";
				case 426:
					return "Upgrade Required";
				case 428:
					return "Precondition Required";
				case 429:
					return "Too Many Requests";
				case 431:
					return "Request Header Fields Too Large";
				case 444:
					return "No Response";
				case 449:
					return "Retry With";
				case 450:
					return "Blocked by Windows Parental Controls";
				case 451:
					return "Unavailable For Legal Reasons";
				case 456:
					return "Unrecoverable Error";
				case 495:
					return "SSL Certificate Error";
				case 496:
					return "SSL Certificate Required";
				case 497:
					return "HTTP Request Sent to HTTPS Port";
				case 499:
					return "Client Closed Request";
				case 500:
					return "Internal Server Error";
				case 501:
					return "Not Implemented";
				case 502:
					return "Bad Gateway ou Proxy Error";
				case 503:
					return "Service Unavailable";
				case 504:
					return "Gateway Time-out";
				case 505:
					return "HTTP Version not supported";
				case 506:
					return "Variant Also Negotiates";
				case 507:
					return "Insufficient storage";
				case 508:
					return "Loop detected";
				case 509:
					return "Bandwidth Limit Exceeded";
				case 510:
					return "Not extended";
				case 511:
					return "Network authentication required";
				case 520:
					return "Unknown Error";
				case 521:
					return "Web Server Is Down";
				case 522:
					return "Connection Timed Out";
				case 523:
					return "Origin Is Unreachable";
				case 524:
					return "A Timeout Occurred";
				case 525:
					return "SSL Handshake Failed";
				case 526:
					return "Invalid SSL Certificate";
				case 527:
					return "Railgun Error";
				default:
					return NULL;
			}
		}

		HTTPResponse::~HTTPResponse()
		{
			delete this->headers;
		}
	}
}
