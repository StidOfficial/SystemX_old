#include "systemx/network/http/httpserver.hpp"

namespace Network
{
	namespace HTTP
	{
		void HTTPServer::OnAccept(int sSocket, sockaddr_in sAddr)
		{
			Network::TCPIPSession *sSession = new HTTP::HTTPSession(sSocket, sAddr);
			auto it = this->sSessions.insert(this->sSessions.begin(), std::pair<int, TCPIPSession*>(sSocket, sSession));
			sSession->Open();
			sSession->Close();
			delete sSession;

			this->sSessions.erase(it);
		}
	}
}
