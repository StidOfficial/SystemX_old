#include "systemx/network/http/httprequest.hpp"

namespace Network
{
	namespace HTTP
	{
		HTTPRequest::HTTPRequest()
		{
			this->headers = new HTTPHeaders();
			this->method = -1;
			this->protocolVersion = -1;
		}

		void HTTPRequest::setMethod(std::string method)
		{
			this->setMethod(HTTPRequest::getMethod(method));
		}

		void HTTPRequest::setMethod(int method)
		{
			this->method = method;
		}

		int HTTPRequest::getMethod()
		{
			return this->method;
		}

		void HTTPRequest::setURI(std::string uri)
		{
			this->URI = uri;
		}

		std::string HTTPRequest::getURI()
		{
			return this->URI;
		}

		void HTTPRequest::setProtocolVersion(std::string protocolVersion)
		{
			this->setProtocolVersion(HTTPProtocol::getProtocolVersion(protocolVersion));
		}

		void HTTPRequest::setProtocolVersion(int protocolVersion)
		{
			this->protocolVersion = protocolVersion;
		}

		int HTTPRequest::getProtocolVersion()
		{
			return this->protocolVersion;
		}
		
		std::string HTTPRequest::getProtocolVersionValue()
		{
			return HTTPProtocol::getProtocolVersion(this->getProtocolVersion());
		}

		HTTPHeaders* &HTTPRequest::getHeaders()
		{
			return this->headers;
		}

		int HTTPRequest::getMethod(std::string method)
		{
			if(method == "GET")
				return HTTP_METHOD_GET;
			else if(method == "HEAD")
				return HTTP_METHOD_HEAD;
			else if(method == "POST")
				return HTTP_METHOD_POST;
			else if(method == "PUT")
				return HTTP_METHOD_PUT;
			else if(method == "DELETE")
				return HTTP_METHOD_DELETE;
			else if(method == "CONNECT")
				return HTTP_METHOD_CONNECT;
			else if(method == "OPTIONS")
				return HTTP_METHOD_OPTIONS;
			else if(method == "TRACE")
				return HTTP_METHOD_TRACE;
			else if(method == "PATCH")
				return HTTP_METHOD_PATCH;

			return -1;
		}

		std::string HTTPRequest::getMethod(int method)
		{
			switch(method)
			{
				case HTTP_METHOD_GET:
					return "GET";
				case HTTP_METHOD_HEAD:
					return "HEAD";
				case HTTP_METHOD_POST:
					return "POST";
				case HTTP_METHOD_PUT:
					return "PUT";
				case HTTP_METHOD_DELETE:
					return "DELETE";
				case HTTP_METHOD_CONNECT:
					return "CONNECT";
				case HTTP_METHOD_OPTIONS:
					return "OPTIONS";
				case HTTP_METHOD_TRACE:
					return "TRACE";
				case HTTP_METHOD_PATCH:
					return "PATCH";
				default:
					return NULL;
			}
		}

		std::string HTTPRequest::getMethodValue()
		{
			return HTTPRequest::getMethod(this->getMethod());
		}

		HTTPRequest::~HTTPRequest()
		{
			delete this->headers;
		}
	}
}
