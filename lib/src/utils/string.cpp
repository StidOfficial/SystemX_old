#include "systemx/utils/string.hpp"

namespace Utils
{
	const std::vector<std::string> String::Split(std::string str, std::string delimiter, int limit)
	{
		std::vector<std::string> sStrings;
		int i = 1;
		size_t fStartPos = 0, fEndPos = 0;
		while(fEndPos != std::string::npos || (limit != -1 && i < limit))
		{
			fEndPos = str.find(delimiter, fStartPos);
			if(fEndPos != std::string::npos)
			{
				sStrings.push_back(str.substr(fStartPos, fEndPos - fStartPos));
				fStartPos = fEndPos + delimiter.length();
			}
			else
				sStrings.push_back(str.substr(fStartPos, str.length() - fStartPos));

			i++;
		}

		return sStrings;
	}
}
