#include <cstring>

#include <openssl/sha.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#include "systemx/utils/crypto.hpp"

namespace Utils
{
	const std::string Crypto::SHA1(const std::string str)
	{
		unsigned char hash[SHA_DIGEST_LENGTH];
		::SHA1((const unsigned char*)str.c_str(), str.length(), hash);

		return std::string((const char*)hash, SHA_DIGEST_LENGTH);
	}

	const std::string Crypto::EncodeBase64(const std::string data)
	{
		BIO *bmem, *b64;
		BUF_MEM *bptr;
		b64 = BIO_new(BIO_f_base64());
		bmem = BIO_new(BIO_s_mem());
		b64 = BIO_push(b64, bmem);
		BIO_write(b64, data.c_str(), data.length());
		BIO_flush(b64);
		BIO_get_mem_ptr(b64, &bptr);
		char* b64Buffer = new char[bptr->length];
		memcpy(b64Buffer, bptr->data, bptr->length - 1);
		b64Buffer[bptr->length - 1] = '\0';
		BIO_free_all(b64);

		std::string base64 = std::string(b64Buffer);
		delete[] b64Buffer;

		return base64;
	}
}
