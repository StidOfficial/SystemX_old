#include <systemx/database/preparedstatement.hpp>
#include <systemx/logger.hpp>
#include <cstring>

namespace Database
{
	PreparedStatement::PreparedStatement(Database* db, sqlite3_stmt* stmt)
	{
		m_db = db;
		m_sqlite_stmt = stmt;
		m_columns = {};

		int column_count = sqlite3_column_count(stmt);
		for(int i = 0; i < column_count; i++)
		{
			const char* column_name = sqlite3_column_name(stmt, i);
			m_columns[column_name] = i;

			LOG_DEBUG("Column name : %s - %d : %d", column_name, i, m_columns.find("address") == m_columns.end());
		}
	}

	void PreparedStatement::Bind(int index, int value)
	{
		if(sqlite3_bind_int(m_sqlite_stmt, index, value) != SQLITE_OK)
			LOG_ERROR("SQL Error : %s", m_db->LastError());
	}

	void PreparedStatement::Bind(int index, std::string value)
	{
		Bind(index, value.c_str());
	}

	void PreparedStatement::Bind(int index, const char* value)
	{
		if(sqlite3_bind_text(m_sqlite_stmt, index, value, -1, SQLITE_STATIC) != SQLITE_OK)
			LOG_ERROR("SQL Error : %s", m_db->LastError());
	}

	/*void Bind(std::string index, std::string value)
	{
		Bind(index.c_str(), value.c_str());
	}

	void Bind(const char* index, const char* value)
	{
		
	}*/

	int PreparedStatement::GetInt(const char* name)
	{
		return sqlite3_column_int(m_sqlite_stmt, m_columns[name]);
	}

	const unsigned char* PreparedStatement::GetString(const char* name)
	{
		return sqlite3_column_text(m_sqlite_stmt, m_columns[name]);
	}

	void PreparedStatement::Execute()
	{
	}

	bool PreparedStatement::First()
	{
		return Next();
	}

	bool PreparedStatement::Next()
	{
		if(m_db->Driver() == DATABASE_DRIVER_SQLITE)
		{
			return sqlite3_step(m_sqlite_stmt) == SQLITE_ROW;
		}

		return false;
	}

	PreparedStatement::~PreparedStatement()
	{
		if(m_db->Driver() == DATABASE_DRIVER_SQLITE)
			sqlite3_finalize(m_sqlite_stmt);
	}
}
