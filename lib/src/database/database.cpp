#include <systemx/database/database.hpp>

#include <systemx/logger.hpp>
#include <iostream>
#include <fstream>

namespace Database
{
	Database::Database(int driver)
	{
		m_driver_db = driver;
	}

	int Database::Driver()
	{
		return m_driver_db;
	}

	const char* Database::LastError()
	{
		return sqlite3_errmsg(sqlite_db);
	}

	void Database::Open(const char* path)
	{
		if(Driver() != DATABASE_DRIVER_SQLITE)
			throw std::runtime_error("Is not SQLite database");

		if(sqlite3_open(path, &sqlite_db) != SQLITE_OK)
			LOG_ERROR("%s", LastError());

		sqlite3_profile(sqlite_db, profile, NULL);
	}

	void Database::Load(const char* path)
	{
		std::ifstream sql_file;
		sql_file.open(path);

		std::string sql_requets = "";
		if(sql_file.is_open())
		{
			std::string line;
			while(getline(sql_file, line))
				sql_requets += line;

			Exec(sql_requets.c_str(), NULL);

			sql_file.close();
		}
		else
			LOG_ERROR("Failed to open : %s", path);
	}

	void Database::Query(const char* sql)
	{
		sqlite3_stmt* stmt;
		sqlite3_prepare(sqlite_db, sql, -1, &stmt, NULL);
		sqlite3_step(stmt);
		LOG_DEBUG("%s", sqlite3_column_text(stmt, 0));
		
		sqlite3_finalize(stmt);
	}

	PreparedStatement* Database::Prepare(const char* sql)
	{
		sqlite3_stmt* stmt;
		if(sqlite3_prepare(sqlite_db, sql, -1, &stmt, NULL) != SQLITE_OK)
		{
			LOG_ERROR("SQL Error : %s", LastError());
			return NULL;
		}

		return new PreparedStatement(this, stmt);
	}

	void Database::Exec(const char* sql, int (*callback)(void*,int,char**,char**))
	{
		if(Driver() == DATABASE_DRIVER_SQLITE)
			if(sqlite3_exec(sqlite_db, sql, callback, NULL, NULL) != SQLITE_OK)
				LOG_ERROR("SQL Error : %s", LastError());
	}

	Database::~Database()
	{
		if(Driver() == DATABASE_DRIVER_SQLITE)
			sqlite3_close(sqlite_db);
	}

	void Database::profile(void* context, const char* sql, sqlite3_uint64 ns)
	{
		LOG_DEBUG("Query : %s", sql);
		LOG_DEBUG("Execution time : %llu ms", ns / 1000000);
	}
}
