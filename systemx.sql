CREATE TABLE IF NOT EXISTS users(id INTEGER PRIMARY KEY, username TEXT NOT NULL, displayName TEXT NOT NULL, password TEXT NOT NULL);
INSERT OR IGNORE INTO users(id, username, displayName, password) VALUES (1, "root", "root", "password123!");
INSERT OR IGNORE INTO users(id, username, displayName, password) VALUES (2, "gaben", "Gaben Newell", "valve");

CREATE TABLE IF NOT EXISTS daemons(id INTEGER PRIMARY KEY, address TEXT NOT NULL, port INTEGER NOT NULL);
INSERT OR IGNORE INTO daemons(id, address, port) VALUES (1, "localhost", 5353);
INSERT OR IGNORE INTO daemons(id, address, port) VALUES (2, "localhost", 5354);
